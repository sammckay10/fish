if status is-interactive
    # Remove welcome message
    set -g fish_greeting

    alias c="clear"

    alias v="nvim"
    alias vi="nvim"
    alias vim="nvim"

    alias gs="git status"
    alias ga="git add ."
    alias gc="git commit -m"
    alias gch="git checkout"
    alias gp="git push"
    alias gpl="git pull"

    function gd
        git diff $argv | delta --side-by-side
    end

    alias n="nvim ~/.config/fish/notes.md"

    fish_add_path ~/.composer/vendor/bin
end
